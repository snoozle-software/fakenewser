/**
 * 
 */
package articler;

import com.datumbox.framework.applications.nlp.TextClassifier;
import com.datumbox.framework.common.Configuration;
import com.datumbox.framework.core.machinelearning.MLBuilder;

/**
 * @author greg
 *
 */
public class DatumVals extends Articler{
	
	// enum
	public enum SENTIMENTS
	{
		NONE(0),
		POSTIVE(1),
		NEGATIVE(-1),
		NEUTRAL(0);
		
		private final int num;
		
		SENTIMENTS(int num)
		{
			this.num = num;
		}
		
		public int getNum()
		{
			return num;
		}
	}
	
	public enum SUBJECTIVITY
	{
		NONE(0),
		SUBJECTIVE(1),
		OBJECTIVE(-1);
		
		private final int num;
		
		SUBJECTIVITY(int num)
		{
			this.num = num;
		}
		
		public int getNum()
		{
			return num;
		}
	}
	
	public enum POS_NEG
	{
		NONE(0),
		POS(1),
		NEG(-1);
		
		private final int num;
		
		POS_NEG(int num)
		{
			this.num = num;
		}
		
		public int getNum()
		{
			return num;
		}
	}
	
	public enum READABILITY
	{
		NONE(0),
		ADVANCED(1),
		INTERMEDIATE(0),
		BASIC(-1);
		
		private final int num;
		
		READABILITY(int num)
		{
			this.num = num;
		}
		
		public int getNum()
		{
			return num;
		}
	}
	
	public enum GENDER
	{
		NONE(0),
		MALE(1),
		FEMALE(-1);
		
		private final int num;
		
		GENDER(int num)
		{
			this.num = num;
		}
		
		public int getNum()
		{
			return num;
		}
	}
	
	
	protected SENTIMENTS articleSentiment = SENTIMENTS.NONE;
	protected SENTIMENTS titleSentiment = SENTIMENTS.NONE;
	protected SUBJECTIVITY titleSubjectivity = SUBJECTIVITY.NONE;
	protected SUBJECTIVITY articleSubjectivity = SUBJECTIVITY.NONE;
	protected POS_NEG titleSpam = POS_NEG.NONE;
	protected POS_NEG articleSpam = POS_NEG.NONE;
	protected POS_NEG titleAdult = POS_NEG.NONE;
	protected POS_NEG articleAdult = POS_NEG.NONE;
	protected READABILITY titleReadability = READABILITY.NONE;
	protected READABILITY articleReadability = READABILITY.NONE;
	protected POS_NEG titleIsCommercial = POS_NEG.NONE;
	protected POS_NEG articleIsCommercial = POS_NEG.NONE;
	protected POS_NEG titleIsEducational = POS_NEG.NONE;
	protected POS_NEG articleIsEducational = POS_NEG.NONE;
	protected GENDER titleGender = GENDER.NONE;
	protected GENDER articleGender = GENDER.NONE;
	protected int articleLength = 0;
	
	Configuration configuration = Configuration.getConfiguration();
	TextClassifier sentimentClassifier = MLBuilder.load(TextClassifier.class, "SentimentAnalysis", configuration);
	TextClassifier subjectiviyClassifier = MLBuilder.load(TextClassifier.class, "SubjectivityAnalysis", configuration);
	TextClassifier spamClassifier = MLBuilder.load(TextClassifier.class, "SpamDetection", configuration);
	TextClassifier adultClassifier = MLBuilder.load(TextClassifier.class, "AdultContent", configuration);
	TextClassifier commercialClassifier = MLBuilder.load(TextClassifier.class, "CommercialDetection", configuration);
	TextClassifier educationalClassifier = MLBuilder.load(TextClassifier.class, "EducationalDetection", configuration);
	TextClassifier genderClassifier = MLBuilder.load(TextClassifier.class, "GenderDetection", configuration);
 	/**
	 * 
	 */
	public DatumVals() {
		
	}
	
	public DatumVals(String articleURL)
	{
		super(articleURL);
		if(this.ableToOpen)
		{
			this.titleSentiment = pullSentiment(title);
			this.articleSentiment = pullSentiment(article);
			this.titleSubjectivity = pullSubjectivity(title);
			this.articleSubjectivity = pullSubjectivity(article);
			this.titleSpam = isSpam(title);
			this.articleSpam = isSpam(article);
			this.titleAdult = isAdult(title);
			this.articleAdult = isAdult(article);
			this.titleReadability = pullReadability(title);
			this.articleReadability = pullReadability(article);
			this.titleIsCommercial = isCommercial(title);
			this.articleIsCommercial = isCommercial(article);
			this.titleIsEducational = isEducational(title);
			this.articleIsEducational = isEducational(article);
			this.titleGender = pullGender(title);
			this.articleGender = pullGender(article);
			this.articleLength = article.length();
		}
	}

	/**
	 * 
	 * @param articler
	 */
	public DatumVals(Articler articler) 
	{
		super(articler.getArticleURL(), articler.getArticle(), articler.getTitle(), articler.ableToOpen);
		if(articler.ableToOpen)
		{
			
			this.titleSentiment = pullSentiment(title);
			this.articleSentiment = pullSentiment(article);
			this.titleSubjectivity = pullSubjectivity(title);
			this.articleSubjectivity = pullSubjectivity(article);
			this.titleSpam = isSpam(title);
			this.articleSpam = isSpam(article);
			this.titleAdult = isAdult(title);
			this.articleAdult = isAdult(article);
			this.titleReadability = pullReadability(title);
			this.articleReadability = pullReadability(article);
			this.titleIsCommercial = isCommercial(title);
			this.articleIsCommercial = isCommercial(article);
			this.titleIsEducational = isEducational(title);
			this.articleIsEducational = isEducational(article);
			this.titleGender = pullGender(title);
			this.articleGender = pullGender(article);
			this.articleLength = article.length();
		}
	}

	
	
	// Getters
	
	public SENTIMENTS getArticleSentiment() {
		return articleSentiment;
	}

	public SENTIMENTS getTitleSentiment() {
		return titleSentiment;
	}

	public SUBJECTIVITY getTitleSubjectivity() {
		return titleSubjectivity;
	}

	public SUBJECTIVITY getArticleSubjectivity() {
		return articleSubjectivity;
	}

	public POS_NEG getTitleSpam() {
		return titleSpam;
	}

	public POS_NEG getArticleSpam() {
		return articleSpam;
	}

	public POS_NEG getTitleAdult() {
		return titleAdult;
	}

	public POS_NEG getArticleAdult() {
		return articleAdult;
	}

	public READABILITY getTitleReadability() {
		return titleReadability;
	}

	public READABILITY getArticleReadability() {
		return articleReadability;
	}

	public POS_NEG getTitleIsCommercial() {
		return titleIsCommercial;
	}

	public POS_NEG getArticleIsCommercial() {
		return articleIsCommercial;
	}

	public POS_NEG getTitleIsEducational() {
		return titleIsEducational;
	}

	public POS_NEG getArticleIsEducational() {
		return articleIsEducational;
	}

	public GENDER getTitleGender() {
		return titleGender;
	}

	public GENDER getArticleGender() {
		return articleGender;
	}

	public int getArticleLength() {
		return articleLength;
	}

	/**
	 * 
	 * @param text
	 * @return
	 */
	private SENTIMENTS pullSentiment(String text)
	{
		SENTIMENTS sentimentRet = SENTIMENTS.NONE;
		
		String predicted = (String) sentimentClassifier.predict(text).getYPredicted();
		
		switch(predicted)
		{
		case "positive":
			sentimentRet = SENTIMENTS.POSTIVE;
			break;
			

		case "negative":
			sentimentRet = SENTIMENTS.NEGATIVE;
			break;
			

		case "neutral":
			sentimentRet = SENTIMENTS.NEUTRAL;
			break;
			
		default:
			sentimentRet = SENTIMENTS.NONE;
			break;
		}
		return sentimentRet;
	}

	/**
	 * 
	 * @param text
	 * @return
	 */
	private SUBJECTIVITY pullSubjectivity(String text)
	{
		SUBJECTIVITY subjectivityRet = SUBJECTIVITY.NONE;
		
		String predicted = (String) subjectiviyClassifier.predict(text).getYPredicted();
		
		switch(predicted)
		{
		case "objective":
			subjectivityRet = SUBJECTIVITY.OBJECTIVE;
			break;
			

		case "subjective":
			subjectivityRet = SUBJECTIVITY.SUBJECTIVE;
			break;
			
		default:
			subjectivityRet = SUBJECTIVITY.NONE;
			break;
		}
		
		return subjectivityRet;
	}
	
	
	/**
	 * 
	 * @param text
	 * @return
	 */
	private POS_NEG isSpam(String text)
	{
		POS_NEG subjectivityRet = POS_NEG.NONE;
		
		String predicted = (String) spamClassifier.predict(text).getYPredicted();
		
		switch(predicted)
		{
		case "spam":
			subjectivityRet = POS_NEG.POS;
			break;
			

		case "nospam":
			subjectivityRet = POS_NEG.NEG;
			break;
			
		default:
			subjectivityRet = POS_NEG.NONE;
			break;
		}
		return subjectivityRet;
	}
	
	/**
	 * 
	 * @param text
	 * @return
	 */
	private POS_NEG isAdult(String text)
	{
		POS_NEG isAdultRet = POS_NEG.NONE;
		
		String predicted = (String) adultClassifier.predict(text).getYPredicted();
		
		switch(predicted)
		{
		case "adult":
			isAdultRet = POS_NEG.POS;
			break;
			

		case "noadult":
			isAdultRet = POS_NEG.NEG;
			break;
			
		default:
			isAdultRet = POS_NEG.NONE;
			break;
		}
		
		return isAdultRet;
	}
	
	/**
	 * 
	 * @param text
	 * @return
	 */
	private READABILITY pullReadability(String text)
	{
		READABILITY readability = READABILITY.NONE;
		
		// fix later
		return readability;
	}
	
	/**
	 * 
	 * @param text
	 * @return
	 */
	private POS_NEG isCommercial(String text)
	{
		POS_NEG isCommerical = POS_NEG.NONE;
		
		String predicted = (String) commercialClassifier.predict(text).getYPredicted();
		switch(predicted)
		{
		case "commercial":
			isCommerical = POS_NEG.POS;
			break;
			

		case "noncommercial":
			isCommerical = POS_NEG.NEG;
			break;
			
		default:
			isCommerical = POS_NEG.NONE;
			break;
		}
		
		return isCommerical;
	}
	
	/**
	 * 
	 * @param text
	 * @return
	 */
	private POS_NEG isEducational(String text)
	{
		POS_NEG isEducation = POS_NEG.NONE;
		
		String predicted = (String) educationalClassifier.predict(text).getYPredicted();
		
		switch(predicted)
		{
		case "educational":
			isEducation = POS_NEG.POS;
			break;
			

		case "noneducational":
			isEducation = POS_NEG.NEG;
			break;
			
		default:
			isEducation = POS_NEG.NONE;
			break;
		}
		
		return isEducation;
	}
	
	/**
	 * 
	 * @param text
	 * @return
	 */
	private GENDER pullGender(String text)
	{
		GENDER gender = GENDER.NONE;
		
		String predicted = (String) genderClassifier.predict(text).getYPredicted();
		switch(predicted)
		{
		case "male":
			gender = GENDER.MALE;
			break;
			

		case "female":
			gender = GENDER.FEMALE;
			break;
			
		default:
			gender = GENDER.NONE;
			break;
		}
		
		return gender;
	}

}
