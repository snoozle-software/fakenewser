package utilities;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.dbcp2.BasicDataSource;



public class DBBroker extends HttpServlet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static BasicDataSource datasource = null;

	public void init() throws ServletException
	{
		System.out.println("Setting up connection pool");
		try {
			URI dbUri = new URI(System.getenv("DATABASE_URL_AWS"));
		
		  String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath();
		  datasource = new BasicDataSource();
		
		  if (dbUri.getUserInfo() != null) {
			  datasource.setUsername(dbUri.getUserInfo().split(":")[0]);
			  datasource.setPassword(dbUri.getUserInfo().split(":")[1]);
		  }
		  datasource.setDriverClassName("org.postgresql.Driver");
		  datasource.setUrl(dbUrl);
		  datasource.setInitialSize(1);
		  datasource.setMaxTotal(Integer.parseInt(System.getenv("NUM_DB_CON")));
		  datasource.setMaxIdle(10);
		  datasource.setMaxWaitMillis(10000);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	  /**
	   * Dole out the connections here.
	   */
	  public static synchronized Connection getConnection() 
	        
	  {
		  Connection con = null;
		  if(datasource != null)
		  {
			try {
				con = datasource.getConnection();
			} catch (SQLException e) {
				System.out.println("Error with pooling connections");
				//e.printStackTrace();
			}
		  }
	    	 
		  
		  return con;
	  }

	  /**
	   * Must close the database connection to return it to the pool.
	   */
	  public static synchronized void freeConnection(Connection connection)
	  {
	    try
	    {
	      connection.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("DBBroker: Threw an exception closing a database connection");
	      e.printStackTrace();
	    }
	  }
}
