/**
 * 
 */
package articler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import articler.DatumValsWTruth.FAKE_NEWS;
import utilities.DBUtilities;

/**
 * @author greg
 *
 */
public class DatabaseAPI {

	public static boolean isExisting(String articleURL) {
		boolean returnVal = false;
		
		String selectionString = "SELECT\n" + 
				"EXISTS (SELECT \n" + 
				"	  1\n" + 
				"	FROM \n" + 
				"	  public.articlescores\n" + 
				"	WHERE \n" + 
				"	  articlescores.articleurl = ?)";
		ResultSet rs = null;
		Connection con = DBUtilities.connectDB();
		
		PreparedStatement pst = null;
		try
		{
			pst = con.prepareStatement(selectionString);
			pst.setString(1, articleURL);
			rs = pst.executeQuery();
			while(rs.next())
			{
				returnVal = rs.getBoolean(1);
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		} 
		finally
		{
			try{
	        	if(pst != null)
	        	{
	        		pst.close();
	        	}

	        	if(rs != null)
	        	{
	        		rs.close();
	        	}
	        	if(con != null)
	        	{
	        		DBUtilities.closeDB(con);
	        	}
        	}
    	    catch (SQLException ex) {
    	         Logger lgr = Logger.getLogger(DatabaseAPI.class.getName());
    	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		}
		}
		
		return returnVal;
	}

	public static int insertDatumValsWTruth(DatumValsWTruth datumValsWTruth) {
		int returnVal = 0;
		
		String insertString = "INSERT INTO public.articlescores(\n" + 
				"       articleurl, \n" + 
				"       titlesentiment, \n" + 
				"       articlesentiment, \n" + 
				"       titlesubjectivity, \n" + 
				"       articlesubjectivity, \n" + 
				"       titlespam, \n" + 
				"	articlespam, \n" + 
				"	titleadult, \n" + 
				"	articleadult, \n" + 
				"	titlereadability, \n" + 
				"	articlereadability, \n" + 
				"	titleiscommercial, \n" + 
				"	articleiscommercial, \n" + 
				"	titleiseducational, \n" + 
				"	articleiseducational, \n" + 
				"	titlegender, \n" + 
				"	articlegender, \n" + 
				"	articlelength, \n" + 
				"	truthvotes, \n" + 
				"	totalvotes )  SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?  WHERE NOT EXISTS(  SELECT articlescores.articleurl FROM public.articlescores WHERE articlescores.articleurl = ?)";
		
		Connection con = DBUtilities.connectDB();
		
		PreparedStatement pst = null;
		try
		{
			pst = con.prepareStatement(insertString);
			pst.setString(1, datumValsWTruth.getArticleURL());
			pst.setInt(2, datumValsWTruth.getTitleSentiment().getNum());
			pst.setInt(3, datumValsWTruth.getArticleSentiment().getNum());
			pst.setInt(4, datumValsWTruth.getTitleSubjectivity().getNum());
			pst.setInt(5, datumValsWTruth.getArticleSubjectivity().getNum());
			pst.setInt(6, datumValsWTruth.getTitleSpam().getNum());
			pst.setInt(7, datumValsWTruth.getArticleSpam().getNum());
			pst.setInt(8, datumValsWTruth.getTitleAdult().getNum());
			pst.setInt(9, datumValsWTruth.getArticleAdult().getNum());
			pst.setInt(10, datumValsWTruth.getTitleReadability().getNum());
			pst.setInt(11, datumValsWTruth.getArticleReadability().getNum());
			pst.setInt(12, datumValsWTruth.getTitleIsCommercial().getNum());
			pst.setInt(13, datumValsWTruth.getArticleIsCommercial().getNum());
			pst.setInt(14, datumValsWTruth.getTitleIsEducational().getNum());
			pst.setInt(15, datumValsWTruth.getArticleIsEducational().getNum());
			pst.setInt(16, datumValsWTruth.getTitleGender().getNum());
			pst.setInt(17, datumValsWTruth.getArticleGender().getNum());
			pst.setInt(18, datumValsWTruth.getArticleLength());
			pst.setInt(19, datumValsWTruth.getTruth()==FAKE_NEWS.NO?1:0);
			pst.setInt(20, 1);
			pst.setString(21, datumValsWTruth.getArticleURL());
			returnVal = pst.executeUpdate();
			
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		} 
		finally
		{
			try{
	        	if(pst != null)
	        	{
	        		pst.close();
	        	}
	        	if(con != null)
	        	{
	        		DBUtilities.closeDB(con);
	        	}
        	}
    	    catch (SQLException ex) {
    	         Logger lgr = Logger.getLogger(DatabaseAPI.class.getName());
    	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		}
		}
		
		return returnVal;
	}

	public static int incrementTruthURL(String articleURL, FAKE_NEWS fakeNews) {
		
		String updateString = "";
		int returnVal = 0;
		if(fakeNews == FAKE_NEWS.NO)
		{
			updateString = "UPDATE public.articlescores " + 
			   "SET truthvotes = truthvotes + 1, totalvotes = totalvotes + 1, updatedat = now() " +
			   "WHERE articleurl = ?";
		}
		else if(fakeNews == FAKE_NEWS.YES)
		{
			updateString = "UPDATE public.articlescores " + 
					   "SET totalvotes = totalvotes + 1, updatedat = now() " +
					   "WHERE articleurl = ?";
		}
		else
		{
			return 0;
		}
		Connection con = DBUtilities.connectDB();
		
		PreparedStatement pst = null;
		try
		{
			pst = con.prepareStatement(updateString);
			pst.setString(1, articleURL);
			returnVal = pst.executeUpdate();
			
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		} 
		finally
		{
			try{
	        	if(pst != null)
	        	{
	        		pst.close();
	        	}
	        	if(con != null)
	        	{
	        		DBUtilities.closeDB(con);
	        	}
        	}
    	    catch (SQLException ex) {
    	         Logger lgr = Logger.getLogger(DatabaseAPI.class.getName());
    	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		}
		}
		
		return returnVal;
	}

	public static ArticleDAO getDAO(String articleURL) {
		
		
		ArticleDAO articleDAO = null;

		String selectionString = "SELECT * FROM articlescores WHERE articleurl LIKE ?";
		ResultSet rs = null;
		Connection con = DBUtilities.connectDB();
		
		PreparedStatement pst = null;
		try
		{
			pst = con.prepareStatement(selectionString);
			pst.setString(1, articleURL);
			rs = pst.executeQuery();
			while(rs.next())
			{
				articleDAO = new ArticleDAO(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4),rs.getInt(5),rs.getInt(6)
						,rs.getInt(7),rs.getInt(8),rs.getInt(9),rs.getInt(10),rs.getInt(11),rs.getInt(12),rs.getInt(13),rs.getInt(14)
						,rs.getInt(15),rs.getInt(16),rs.getInt(17),rs.getInt(18),rs.getInt(19),rs.getInt(20),rs.getInt(21)
						,rs.getTimestamp(22), rs.getTimestamp(23));
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		} 
		finally
		{
			try{
	        	if(pst != null)
	        	{
	        		pst.close();
	        	}

	        	if(rs != null)
	        	{
	        		rs.close();
	        	}
	        	if(con != null)
	        	{
	        		DBUtilities.closeDB(con);
	        	}
        	}
    	    catch (SQLException ex) {
    	         Logger lgr = Logger.getLogger(DatabaseAPI.class.getName());
    	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		}
		}
		
		
		return articleDAO;
	}
	
	

}
