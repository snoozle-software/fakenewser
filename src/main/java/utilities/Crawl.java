package utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Crawl {
	
	// gets the doc class for crawl
	public static Document getDocument(String url)
	{
		Document doc = null;
		boolean openDoc = false;
		int tries = 0;
		final int maxTries = 3;
		do{
			try
			{
				Connection conn = Jsoup.connect(url).maxBodySize(0).timeout(10000);
				Response response = conn.execute();
				int statusCode = response.statusCode();
				if(statusCode == 200)
				{
					try
					{
						doc = conn.get();
						openDoc = true;
					}
					catch(java.net.SocketTimeoutException e)
					{
						System.out.println("utilities.Crawl: socketTimeException: couldn't open " + url);
						openDoc = false;
					}
					
				}
				else
				{
					System.out.print("Got a " + statusCode + " code for " + url);
				}
			}
			catch(SocketTimeoutException e)
			{
				System.out.println("Time out exception for " + url + " Try " + tries);
				if(tries >= maxTries)
				{
					return null;
				}
				else
				{
					tries++;
				}
			} catch (IOException e) {
				System.out.println("Error crawler: Could not access " + url);
				if(tries >= maxTries)
				{
					return null;
				}
				else
				{
					tries++;
				}
			}
		}while(!openDoc);
		
		return doc;
	}
	
	public static Document getDocument(String url, HashMap<String,String> data)
	{
		Document doc = null;
		boolean openDoc = false;
		int tries = 0;
		final int maxTries = 3;
		do{
			try
			{
				Connection conn = Jsoup.connect(url).ignoreContentType(true).maxBodySize(0).timeout(10000);
				for(String key : data.keySet())
				{
					conn = conn.data(key,data.get(key));
				}
				Response response = conn.execute();
				int statusCode = response.statusCode();
				if(statusCode == 200)
				{
					doc = conn.post();
					openDoc = true;
				}
				else
				{
					System.out.print("Got a " + statusCode + " code for " + url);
				}
			}
			catch(SocketTimeoutException e)
			{
				System.out.println("Time out exception for " + url + " Try " + tries);
				if(tries >= maxTries)
				{
					return null;
				}
				else
				{
					tries++;
				}
			} catch (IOException e) {
				System.out.println("Error crawler: Could not access " + url);
				if(tries >= maxTries)
				{
					return null;
				}
				else
				{
					tries++;
				}
			}
		}while(!openDoc);
		
		return doc;
	}
	
	public static String sendPostRequest(String requestUrl, String payload) {
		StringBuffer jsonString = new StringBuffer();
		
		try {
	        URL url = new URL(requestUrl);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

	        connection.setDoInput(true);
	        connection.setDoOutput(true);
	        connection.setRequestMethod("POST");
	        connection.setRequestProperty("Accept", "application/json");
	        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
	        writer.write(payload);
	        writer.close();
	        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	        jsonString = new StringBuffer();
	        String line;
	        while ((line = br.readLine()) != null) {
	                jsonString.append(line);
	        }
	        br.close();
	        connection.disconnect();
	    } catch (Exception e) {
	            throw new RuntimeException(e.getMessage());
	    }
	    return jsonString.toString();
	}
	
}
