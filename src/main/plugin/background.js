// Copyright 2018 Snoozle Sports

'use strict';

chrome.runtime.onInstalled.addListener(function() {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
	    chrome.declarativeContent.onPageChanged.addRules([{
	      conditions: [
	        // When a page contains a <article> tag...
	        new chrome.declarativeContent.PageStateMatcher({
	          css: ["article"]
	        })
	      ],
	      // ... show the page action.
	      actions: [new chrome.declarativeContent.ShowPageAction() ]
	    }]);
	  });  
});
