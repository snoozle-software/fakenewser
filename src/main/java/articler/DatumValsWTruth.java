/**
 * 
 */
package articler;

/**
 * @author greg
 *
 */
public class DatumValsWTruth extends DatumVals {
	/**
	 * 
	 * @author greg
	 *
	 */
	public enum FAKE_NEWS
	{
		YES(1),
		NEUTRAL(0),
		NO(-1),
		NONE(0);
		
		private final int num;
		
		FAKE_NEWS(int num)
		{
			this.num = num;
		}
		
		public int getNum()
		{
			return this.num;
		}
	}
	
	protected FAKE_NEWS truth = FAKE_NEWS.NONE;
	
	/**
	 * Constructors
	 */
	
	
	public DatumValsWTruth()
	{
		super();
	}
	
	public DatumValsWTruth(String articleURL, FAKE_NEWS truth)
	{
		super(articleURL);
		
		this.truth = truth;
		
	}

	public FAKE_NEWS getTruth() {
		return truth;
	}

	public void setTruth(FAKE_NEWS truth) {
		this.truth = truth;
	}
	
	
}
