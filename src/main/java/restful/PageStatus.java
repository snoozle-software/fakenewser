package restful;

import articler.ArticleDAO;
import articler.DatumVals.GENDER;
import articler.DatumVals.POS_NEG;
import articler.DatumVals.READABILITY;
import articler.DatumVals.SENTIMENTS;
import articler.DatumVals.SUBJECTIVITY;
import articler.DatumValsWTruth.FAKE_NEWS;

public class PageStatus {
	// properties
	private int isFakeNews = FAKE_NEWS.NONE.getNum();
	protected int articleSentiment = SENTIMENTS.NONE.getNum();
	protected int articleSubjectivity = SUBJECTIVITY.NONE.getNum();
	protected int articleSpam = POS_NEG.NONE.getNum();
	protected int articleAdult = POS_NEG.NONE.getNum();
	protected int articleReadability = READABILITY.NONE.getNum();
	protected int articleIsCommercial = POS_NEG.NONE.getNum();
	protected int articleIsEducational = POS_NEG.NONE.getNum();
	protected int articleGender = GENDER.NONE.getNum();
	
	// constants
	private static final double consensusThres = 0.75;
	private static final int minVotes = 1;
	
	public PageStatus()
	{
		
	}
	
	public PageStatus(String pageUrl)
	{
		
	}
	
	public PageStatus(ArticleDAO articleDAO) 
	{
		double truthRatio = (double)(articleDAO.getTruthVotes())/(double)(articleDAO.getTotalVotes());
		if(articleDAO.getTotalVotes() >= minVotes)
		{
			if(truthRatio > consensusThres)
			{
				this.isFakeNews = FAKE_NEWS.NO.getNum();
			}
			else if(truthRatio <= (1-consensusThres))
			{
				this.isFakeNews = FAKE_NEWS.YES.getNum();
			}
			else
			{
				this.isFakeNews = FAKE_NEWS.NEUTRAL.getNum();
			}
		}
		else
		{
			this.isFakeNews = FAKE_NEWS.NEUTRAL.getNum();
		}
		this.articleSentiment = articleDAO.getArticleSentiment();
		this.articleSubjectivity = articleDAO.getArticleSubjectivity();
		this.articleSpam = articleDAO.getArticleSpam();
		this.articleAdult = articleDAO.getArticleAdult();
		this.articleReadability = articleDAO.getArticleReadability();
		this.articleIsCommercial = articleDAO.getArticleCommercial();
		this.articleIsEducational = articleDAO.getArticleEducational();
		this.articleGender = articleDAO.getArticleGender();
	}

	public int getIsFakeNews()
	{
		return this.isFakeNews;
	}

	public int getArticleSentiment() {
		return articleSentiment;
	}

	public int getArticleSubjectivity() {
		return articleSubjectivity;
	}

	public int getArticleSpam() {
		return articleSpam;
	}

	public int getArticleAdult() {
		return articleAdult;
	}

	public int getArticleReadability() {
		return articleReadability;
	}

	public int getArticleIsCommercial() {
		return articleIsCommercial;
	}

	public int getArticleIsEducational() {
		return articleIsEducational;
	}

	public int getArticleGender() {
		return articleGender;
	}
}
