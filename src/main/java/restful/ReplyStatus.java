package restful;

public class ReplyStatus {
	
	// properties
	private int returnVal = -1;
	
	public ReplyStatus()
	{
		
	}
	
	public ReplyStatus(int returnVal)
	{
		this.returnVal = returnVal;
	}
	
	public int getReplyStatus()
	{
		return this.returnVal;
	}
	
}
