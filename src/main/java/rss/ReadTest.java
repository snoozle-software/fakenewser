package rss;

import java.text.ParseException;
import java.util.ArrayList;

import articler.DatabaseAPI;
import articler.DatumValsWTruth;
import articler.DatumValsWTruth.FAKE_NEWS;


public class ReadTest {
	public static void main(String[] args) throws ParseException {
		
	    ArrayList<FeedDB> feeds = new ArrayList<FeedDB>();
	    feeds.add(new FeedDB("http://rss.cnn.com/rss/cnn_allpolitics.rss"));
	    FAKE_NEWS fakeNews = FAKE_NEWS.NO;
	    for (FeedDB feedDB : feeds)
	    {
	    	RSSFeedParser parser = new RSSFeedParser(feedDB.getFeed());
	    	
	    	Feed feed = parser.readFeed();
	    	for (FeedMessage message : feed.getMessages()) {
	    		String articleURL = message.getLink();
    			boolean existingPage = DatabaseAPI.isExisting(articleURL);
    	    	boolean pageAbleToOpen = false;
    	    	if(!existingPage)
    	    	{
    	    		DatumValsWTruth datumValsWTruth = new DatumValsWTruth(articleURL, fakeNews);
    	    		pageAbleToOpen = datumValsWTruth.isAbleToOpen();
    	    		if(pageAbleToOpen)
    	    		{
    	    			DatabaseAPI.insertDatumValsWTruth(datumValsWTruth);
    	    		}
    	    	}
		    }
	    }

	}
}
