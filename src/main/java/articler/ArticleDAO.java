package articler;

import java.util.Date;

public class ArticleDAO {

	protected int id = 0;
	protected String articleURL = "";
	protected int titleSentiment = 0;
	protected int articleSentiment = 0;
	protected int titleSubjectivity = 0;
	protected int articleSubjectivity = 0;
	protected int titleSpam = 0;
	protected int articleSpam = 0;
	protected int titleAdult = 0;
	protected int articleAdult = 0;
	protected int titleReadability = 0;
	protected int articleReadability = 0;
	protected int titleCommercial = 0;
	protected int articleCommercial = 0;
	protected int titleEducational = 0;
	protected int articleEducational = 0;
	protected int titleGender = 0;
	protected int articleGender = 0;
	protected int articleLength = 0;
	protected int truthVotes = 0;
	protected int totalVotes = 0;
	protected Date updatedAt = new Date();
	protected Date createdAt = new Date();
	
	public ArticleDAO()
	{
		
	}
	
	public ArticleDAO(String url)
	{
		this(DatabaseAPI.getDAO(url));
	}
	
	public ArticleDAO(int id, String articleURL, int titleSentiment, int articleSentiment, int titleSubjectivity,
			int articleSubjectivity, int titleSpam, int articleSpam, int titleAdult, int articleAdult,
			int titleReadability, int articleReadability, int titleCommercial, int articleCommercial,
			int titleEducational, int articleEducational, int titleGender, int articleGender, int articleLength,
			int truthVotes, int totalVotes, Date updatedAt, Date createdAt) {
		super();
		this.id = id;
		this.articleURL = articleURL;
		this.titleSentiment = titleSentiment;
		this.articleSentiment = articleSentiment;
		this.titleSubjectivity = titleSubjectivity;
		this.articleSubjectivity = articleSubjectivity;
		this.titleSpam = titleSpam;
		this.articleSpam = articleSpam;
		this.titleAdult = titleAdult;
		this.articleAdult = articleAdult;
		this.titleReadability = titleReadability;
		this.articleReadability = articleReadability;
		this.titleCommercial = titleCommercial;
		this.articleCommercial = articleCommercial;
		this.titleEducational = titleEducational;
		this.articleEducational = articleEducational;
		this.titleGender = titleGender;
		this.articleGender = articleGender;
		this.articleLength = articleLength;
		this.truthVotes = truthVotes;
		this.totalVotes = totalVotes;
		this.updatedAt = updatedAt;
		this.createdAt = createdAt;
	}

	public ArticleDAO(ArticleDAO dao) {
		this.id = dao.id;
		this.articleURL = dao.articleURL;
		this.titleSentiment = dao.titleSentiment;
		this.articleSentiment = dao.articleSentiment;
		this.titleSubjectivity = dao.titleSubjectivity;
		this.articleSubjectivity = dao.articleSubjectivity;
		this.titleSpam = dao.titleSpam;
		this.articleSpam = dao.articleSpam;
		this.titleAdult = dao.titleAdult;
		this.articleAdult = dao.articleAdult;
		this.titleReadability = dao.titleReadability;
		this.articleReadability = dao.articleReadability;
		this.titleCommercial = dao.titleCommercial;
		this.articleCommercial = dao.articleCommercial;
		this.titleEducational = dao.titleEducational;
		this.articleEducational = dao.articleEducational;
		this.titleGender = dao.titleGender;
		this.articleGender = dao.articleGender;
		this.articleLength = dao.articleLength;
		this.truthVotes = dao.truthVotes;
		this.totalVotes = dao.totalVotes;
		this.updatedAt = dao.updatedAt;
		this.createdAt = dao.createdAt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArticleURL() {
		return articleURL;
	}

	public void setArticleURL(String articleURL) {
		this.articleURL = articleURL;
	}

	public int getTitleSentiment() {
		return titleSentiment;
	}

	public void setTitleSentiment(int titleSentiment) {
		this.titleSentiment = titleSentiment;
	}

	public int getArticleSentiment() {
		return articleSentiment;
	}

	public void setArticleSentiment(int articleSentiment) {
		this.articleSentiment = articleSentiment;
	}

	public int getTitleSubjectivity() {
		return titleSubjectivity;
	}

	public void setTitleSubjectivity(int titleSubjectivity) {
		this.titleSubjectivity = titleSubjectivity;
	}

	public int getArticleSubjectivity() {
		return articleSubjectivity;
	}

	public void setArticleSubjectivity(int articleSubjectivity) {
		this.articleSubjectivity = articleSubjectivity;
	}

	public int getTitleSpam() {
		return titleSpam;
	}

	public void setTitleSpam(int titleSpam) {
		this.titleSpam = titleSpam;
	}

	public int getArticleSpam() {
		return articleSpam;
	}

	public void setArticleSpam(int articleSpam) {
		this.articleSpam = articleSpam;
	}

	public int getTitleAdult() {
		return titleAdult;
	}

	public void setTitleAdult(int titleAdult) {
		this.titleAdult = titleAdult;
	}

	public int getArticleAdult() {
		return articleAdult;
	}

	public void setArticleAdult(int articleAdult) {
		this.articleAdult = articleAdult;
	}

	public int getTitleReadability() {
		return titleReadability;
	}

	public void setTitleReadability(int titleReadability) {
		this.titleReadability = titleReadability;
	}

	public int getArticleReadability() {
		return articleReadability;
	}

	public void setArticleReadability(int articleReadability) {
		this.articleReadability = articleReadability;
	}

	public int getTitleCommercial() {
		return titleCommercial;
	}

	public void setTitleCommercial(int titleCommercial) {
		this.titleCommercial = titleCommercial;
	}

	public int getArticleCommercial() {
		return articleCommercial;
	}

	public void setArticleCommercial(int articleCommercial) {
		this.articleCommercial = articleCommercial;
	}

	public int getTitleEducational() {
		return titleEducational;
	}

	public void setTitleEducational(int titleEducational) {
		this.titleEducational = titleEducational;
	}

	public int getArticleEducational() {
		return articleEducational;
	}

	public void setArticleEducational(int articleEducational) {
		this.articleEducational = articleEducational;
	}

	public int getTitleGender() {
		return titleGender;
	}

	public void setTitleGender(int titleGender) {
		this.titleGender = titleGender;
	}

	public int getArticleGender() {
		return articleGender;
	}

	public void setArticleGender(int articleGender) {
		this.articleGender = articleGender;
	}

	public int getArticleLength() {
		return articleLength;
	}

	public void setArticleLength(int articleLength) {
		this.articleLength = articleLength;
	}

	public int getTruthVotes() {
		return truthVotes;
	}

	public void setTruthVotes(int truthVotes) {
		this.truthVotes = truthVotes;
	}

	public int getTotalVotes() {
		return totalVotes;
	}

	public void setTotalVotes(int totalVotes) {
		this.totalVotes = totalVotes;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	
	
}
