package rss;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import articler.DatabaseAPI;
import articler.DatumValsWTruth;
import articler.DatumValsWTruth.FAKE_NEWS;

public class PullRSS {

	public static void main(String[] args) {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/dd/MM HH:mm:ss");
		String timeStamp = sdf.format(c.getTime());
		System.out.println("Started at " + timeStamp);
		ArrayList<FeedDB> feeds = new ArrayList<FeedDB>();
		String inputFile = "";
		if(args.length == 0)
		{
			inputFile = "rssLinks.dat";
		}
		else if(args.length == 1)
		{
			inputFile = args[0];
		}
		File file = new File(inputFile);

	    try {

	        Scanner sc = new Scanner(file);

	        while (sc.hasNextLine()) {
	            String rssURL = sc.nextLine();
	            System.out.println(rssURL);
	            feeds.add(new FeedDB(rssURL));
	        }
	        sc.close();
	    } 
	    catch (FileNotFoundException e) {
	        e.printStackTrace();
	    }
		
	    
	    FAKE_NEWS fakeNews = FAKE_NEWS.NO;
	    for (FeedDB feedDB : feeds)
	    {
	    	RSSFeedParser parser = new RSSFeedParser(feedDB.getFeed());
	    	
	    	Feed feed = null;
	    	
	    	try
	    	{
	    		feed = parser.readFeed();
	    		System.out.println("Reading feed: " + feedDB.getFeed());
	    	}
	    	catch( RuntimeException e)
	    	{
	    		System.out.println("Error for feed: " + feedDB.getFeed());
	    		continue;
	    	}
	    	
	    	for (FeedMessage message : feed.getMessages()) {
	    		String articleURL = message.getLink();
    			boolean existingPage = DatabaseAPI.isExisting(articleURL);
    	    	boolean pageAbleToOpen = false;
    	    	if(!existingPage)
    	    	{

    	    		System.out.println("Scannning: " + articleURL);
    	    		DatumValsWTruth datumValsWTruth = new DatumValsWTruth(articleURL, fakeNews);
    	    		pageAbleToOpen = datumValsWTruth.isAbleToOpen();
    	    		if(pageAbleToOpen)
    	    		{
    	    			DatabaseAPI.insertDatumValsWTruth(datumValsWTruth);
    	    		}
    	    	}
    	    	else
    	    	{
    	    		System.out.println("Skipping: " + articleURL);
    	    	}
    	    	
		    }
	    }
	    c = Calendar.getInstance();
		timeStamp = sdf.format(c.getTime());
		System.out.println("Stopping... "+timeStamp);

	}

}
