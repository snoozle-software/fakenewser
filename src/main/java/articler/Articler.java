package articler;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Articler {
	
	// fields
	protected String articleURL = "";
	protected String article = "";
	protected String title = "";
	protected boolean ableToOpen = false;
	
	// Constructors
	public Articler() {
		super();
	}
	
	protected Articler(String articleURL, String article, String title, boolean ableToOpen) {
		super();
		this.articleURL = articleURL;
		this.article = article;
		this.title = title;
		this.ableToOpen = ableToOpen;
	}
	
	//mutatators
	public static String cleanupURL(String articleURL)
	{
		String returnURL = articleURL;
		int findQuest = articleURL.indexOf("?");
		if(findQuest > 0)
		{
			returnURL = articleURL.substring(0,articleURL.indexOf("?"));
		}
		
		
		
		return returnURL;
	}



	public Articler(String articleURL) {
		super();
		this.articleURL = articleURL;
		extractArticle();
	}
	
	// Getters and setters

	public String getArticleURL() {
		return articleURL;
	}


	public void setArticleURL(String articleURL) {
		this.articleURL = articleURL;
		extractArticle();
	}

	public String getArticle() {
		return article;
	}
	
	public String getTitle() {
		return title;
	}

	public boolean isAbleToOpen() {
		return ableToOpen;
	}

	
	// affectors
	

	private void extractArticle() {
		
		Document doc = utilities.Crawl.getDocument(articleURL);
		if(doc != null)
		{
			
			Elements titles = doc.select("title");
			if(titles.size() > 0)
			{
				this.title = titles.first().text();
			}
			
			Elements articles = doc.select("article");
			if(articles.size() > 0)
			{
				this.article = articles.get(0).text();
				this.ableToOpen = true;
			}
			//todo: add case when not article
//			else
//			{
//				this.article = doc.select("body").first().text();
//			}
		}
		else
		{
			this.ableToOpen = false;
		}
		
	}

	public static String removeArgs(String articleURL) {
		String returnString = articleURL;
		
		int locOfQuestMark = returnString.indexOf('?');
		if(locOfQuestMark != -1)
		{
			returnString = returnString.substring(0, locOfQuestMark);
		}
		
		return returnString;
	}


	

		
	
	
}
