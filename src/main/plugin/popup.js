
(function() {
		let fakeNews = document.getElementById('fakenews');
		let realNews = document.getElementById('realnews');
		var url = "";
		var title = "";
		
		fakeNews.onclick = function(element) {
			$.ajax({
		        url: "http://localhost:5001/articler/urlsubmission/",
		        dataType:"json",
		        async: false,
		        data: {"article_url": url,
		      	  	   "isFakeNews":"yes",
		      	    },
		        
		        success: function(repData)
		        {
		        	var test = repData;
		        	$("#fakenews").attr("disabled", "disabled");
		        	$("#realnews").attr("disabled", "disabled");
		        }
		   });
		};
		
		realNews.onclick = function(element) {
			$.ajax({
		        url: "http://localhost:5001/articler/urlsubmission/",
		        dataType:"json",
		        async: false,
		        data: {"article_url": url,
		      	  	   "isFakeNews":"no",
		      	    },
		        
		        success: function(repData)
		        {
		        	var test = repData;
		        	$("#fakenews").attr("disabled", "disabled");
		        	$("#realnews").attr("disabled", "disabled");
		        }
		   });
		};
		  

		chrome.tabs.getSelected(null,function(tab) { // null defaults to current window
		   title = tab.title;
		   url = tab.url;
		   $("#pageTitle").html("<h2>"+title+"</h2>")
		   
		   $.ajax({
		        url: "http://localhost:5001/articler/pageStatus/",
		        dataType:"json",
		        async: false,
		        method:"post",
		        data: {"article_url": url,
		      	    },
		        
		        success: function(repData)
		        {
		        	var determination = "Unknown if fake or real news";
		        	if(repData.isFakeNews == +1)
		        	{
		        		var justificationString = [];
		        		determination = "Likely Fake News";
		        		var justCounter = 0;
		        		if(repData.articleSubjectivity == +1)
		        		{
		        			justificationString[justCounter] = "subjective";
		        			justCounter++;
		        		}
		        		if(repData.articleSpam = +1)
		        		{
		        			justificationString[justCounter] = "spam-y";
		        			justCounter++;
		        		}
		        		if(repData.articleAdult = +1)
		        		{
		        			justificationString[justCounter] = "adult content";
		        			justCounter++;
		        		}
		        		if(repData.articleIsCommercial = +1)
		        		{
		        			justificationString[justCounter] = "commerical content";
		        			justCounter++;
		        		}
		        		var justificationOut = "";
		        		if(justCounter == 1)
		        		{
		        			justificationOut = justificationString[0];
		        		}
		        		else if(justCounter > 1)
		        		{
		        			justificationOut += "|"
		        			for(ii = 0; ii < justCounter; ii++)
			        		{
		        				justificationOut += justificationString[ii] + "|"
			        		}
		        		}
		        		
		        		$("#justification").html("<center><h4>"+justificationOut+"</h4></center>");
		        	}
		        	else if(repData.isFakeNews == -1)
		        	{
		        		determination = "Likely Real News";
		        	}
		        	$("#determination").html("<h3><center>"+determination+"</center></h3>");
		        }
		   });
		});
		
}());
 
