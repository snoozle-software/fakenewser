package rss;

// class for holding the DB info
public class FeedDB {

	// attributes
	private String feed;
	private int category;
	
	// constructors
	public FeedDB()
	{
		this.feed = "";
		this.category = -1;
	}
	
	public FeedDB(String feed)
	{
		this.feed = feed;
	}
	
	public FeedDB(String feed, int category)
	{
		this.feed = feed;
		this.category = category;
	}

	// getters and setters
	public String getFeed() {
		return feed;
	}

	public void setFeed(String feed) {
		this.feed = feed;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}
	
	
}
