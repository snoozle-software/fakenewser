package articler;

import articler.DatumValsWTruth.FAKE_NEWS;
import restful.PageStatus;

/**
 * 
 * @author greg
 *
 */
public class Driver {
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		String articleURL = "";
		FAKE_NEWS fakeNews = FAKE_NEWS.NO;
		if(args.length == 0)
		{
			articleURL = "https://www.cnn.com/2018/10/10/politics/kavanaugh-complaints-judicial-misconduct-john-roberts-transfer/index.html";
		}
		else if(args.length == 1)
		{
			articleURL = args[0];
		}
		else if(args.length == 2)
		{
			articleURL = args[0];
			if(args[1].charAt(0) == 'y')
			{
				fakeNews = FAKE_NEWS.YES;
			}
		}
		else if(args.length > 1)
		{
			System.out.println("Incorrect number of arguments");
		}
		
		articleURL = Articler.removeArgs(articleURL);
		
		//DatumValsWTruth datumVals = new DatumValsWTruth(articleURL, fakeNews);
		boolean existingPage = DatabaseAPI.isExisting(articleURL);
    	boolean pageAbleToOpen = false;
    	if(!existingPage)
    	{
    		DatumValsWTruth datumValsWTruth = new DatumValsWTruth(articleURL, fakeNews);
    		pageAbleToOpen = datumValsWTruth.isAbleToOpen();
    		if(pageAbleToOpen)
    		{
    			DatabaseAPI.insertDatumValsWTruth(datumValsWTruth);
    		}
    	}
    	else
    	{
    		ArticleDAO articleDAO = DatabaseAPI.getDAO(articleURL);
    		PageStatus pageStatus = new PageStatus(articleDAO);
    		DatabaseAPI.incrementTruthURL(articleURL, fakeNews);
    	}
	}
}
