package restful;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import articler.DatumValsWTruth.FAKE_NEWS;
import articler.ArticleDAO;
import articler.Articler;
import articler.DatabaseAPI;
import articler.DatumValsWTruth;

@Controller
public class ArticlerController {

	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
 
        String message = "Hello Spring Boot + JSP";
 
        model.addAttribute("message", message);
 
        return "index";
    }
	
	@RequestMapping(value = { "/image/fn_icon_16.png" }, method = RequestMethod.GET)
    public String favicon(Model model) {
 
        String message = "Hello Spring Boot + JSP";
 
        model.addAttribute("message", message);
        return "redirect:/image/fn_icon_16.png";
    }
	
    @RequestMapping("/articler/urlsubmission")
    @ResponseBody
    public ReplyStatus urlSubmission(@RequestParam(value="article_url", defaultValue="") String articleURL, 
    		@RequestParam(value="isFakeNews", defaultValue="") String isFakeNews) {
    	
    	// Setups fake news
    	FAKE_NEWS fakeNews = FAKE_NEWS.NONE;
    	if(isFakeNews.equalsIgnoreCase("yes"))
    	{
    		fakeNews = FAKE_NEWS.YES;
    	}
    	if(isFakeNews.equalsIgnoreCase("no"))
    	{
    		fakeNews = FAKE_NEWS.NO;
    	}
    	
    	// checking for 
    	articleURL = Articler.cleanupURL(articleURL);
    	boolean existingPage = DatabaseAPI.isExisting(articleURL);
    	boolean pageAbleToOpen = false;
    	if(!existingPage)
    	{
    		DatumValsWTruth datumValsWTruth = new DatumValsWTruth(articleURL, fakeNews);
    		pageAbleToOpen = datumValsWTruth.isAbleToOpen();
    		if(pageAbleToOpen)
    		{
    			DatabaseAPI.insertDatumValsWTruth(datumValsWTruth);
    		}
    	}
    	else
    	{
    		DatabaseAPI.incrementTruthURL(articleURL, fakeNews);
    	}
        
        int status = -1;
        if(existingPage || pageAbleToOpen)
        {
        	status = 0;
        }
        else
        {
        	status = -1;
        }
        
    	return new ReplyStatus(status);
    }
    
    @RequestMapping(value = "/articler/pageStatus",method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> pageStatus(@RequestParam(value="article_url", defaultValue="") String articleURL) 
    {
    	articleURL = Articler.cleanupURL(articleURL);
    	Map<String, Object> map = new HashMap<String, Object>();
    	PageStatus status = new PageStatus(articleURL);
    	ArticleDAO articleDAO = DatabaseAPI.getDAO(articleURL);
    	if(articleDAO != null)
    	{
    		status = new PageStatus(articleDAO);
    	}
    	map.put("isFakeNews", status.getIsFakeNews());
    	map.put("articleSentiment", status.getArticleSentiment());
    	map.put("articleSubjectivity", status.getArticleSubjectivity());
    	map.put("articleSpam", status.getArticleSpam());
    	map.put("articleAdult", status.getArticleAdult());
    	map.put("articleReadability", status.getArticleReadability());
    	map.put("articleIsCommercial", status.articleIsCommercial);
    	map.put("articleIsEducational", status.articleIsEducational);
    	map.put("articleGender", status.getArticleGender());
    	return map;
    }
}
