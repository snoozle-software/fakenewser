package neuralnet;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;

import org.tensorflow.Graph;
import org.tensorflow.Session;
import org.tensorflow.Tensor;
import org.tensorflow.TensorFlow;

public class Driver {
  public static void main(String[] args) throws Exception {
    try (Graph g = new Graph()) {
      final String value = "Hello from " + TensorFlow.version();

      // Construct the computation graph with a single operation, a constant
      // named "MyConst" with a value "value".
      try (Tensor t = Tensor.create(value.getBytes("UTF-8"))) {
        // The Java API doesn't yet include convenience functions for adding operations.
        g.opBuilder("Const", "MyConst").setAttr("dtype", t.dataType()).setAttr("value", t).build();
      }

      // Execute the "MyConst" operation in a Session.
      try (Session s = new Session(g);
          // Generally, there may be multiple output tensors,
          // all of them must be closed to prevent resource leaks.
          Tensor output = s.runner().fetch("MyConst").run().get(0)) {
        System.out.println(new String(output.bytesValue(), "UTF-8"));
      }
      
      DoubleBuffer db = DoubleBuffer.allocate(8);
      db.put(new double[] {0.0, 0.0, 
    		               1.0, 1.0, 
    		               0.0, 1.0, 
    		               1.0, 0.0});
      long[] inShape = {4,2};
      Tensor inputs = Tensor.create(inShape, db);
      db = DoubleBuffer.allocate(4);
      db.put(new double [] {-1.0, 
    		                -1.0,
    		                 1.0,
    		                 1.0});
      long[] outShape = {4,1};
      Tensor output = Tensor.create(outShape);
    }
  }
}
